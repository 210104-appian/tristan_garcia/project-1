package dev.garcia.services;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserServiceTests {

	private UserService userService = new UserService();
	
	@Test
	public void testEmployeeWithFakeData(){
		int expected = -1;
		int actual = userService.isEmployee("Fake", "Name");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testManagerWithFakeData(){
		boolean expected = false;
		boolean actual = userService.isManager("Fake", "Name");
		assertEquals(expected, actual);
	}
	

}
