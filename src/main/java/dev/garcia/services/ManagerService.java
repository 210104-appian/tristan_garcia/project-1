package dev.garcia.services;

import java.util.ArrayList;

import dev.garcia.daos.ManagerDao;
import dev.garcia.daos.ManagerDaoImpl;
import dev.garcia.models.Employee;
import dev.garcia.models.detailedRequest;


public class ManagerService {
	private ManagerDao managerDao = new ManagerDaoImpl();
	
	public ArrayList<detailedRequest> allPendingView(){
		return managerDao.allPendingView();
	}
	public void updateStatusResolved(int requestID) {
		managerDao.updateStatusResolved(requestID);
	}
	public void updateStatusDenied(int requestID) {
		managerDao.updateStatusDenied(requestID);
	}
	public ArrayList<detailedRequest> allResolvedView(){
		return managerDao.allResolvedView();
	}
	/*
	public ArrayList<Employee> employeeView(){
		return managerDao.employeeView();
	}
	*/
}
