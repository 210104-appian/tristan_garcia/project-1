package dev.garcia.models;

public class detailedRequest {
	
	private String username;
	private String fname;
	private String lname;
	private int requestID;
	private double amount;
	private String requestStatus;

	public detailedRequest() {
		super();
	}
	
	public detailedRequest(String username, String fname, String lname, int requestID, double amount,String requestStatus) {
		super();
		this.username = username;
		this.fname = fname;
		this.lname = lname;
		this.requestID = requestID;
		this.amount = amount;
		this.requestStatus = requestStatus;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public int getRequestID() {
		return requestID;
	}

	public void setRequestID(int requestID) {
		this.requestID = requestID;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	


}
