package dev.garcia.models;

public class Request {
	private int id;
	private double amount;
	private String requestStatus;
	
	public Request(){
		super();
	}
	
	public Request(int id, double amount, String requestStatus){
		this.id = id;
		this.amount = amount;
		this.requestStatus = requestStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	
}
