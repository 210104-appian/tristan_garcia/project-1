package dev.garcia.servlets;

import java.io.BufferedReader;
import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.juli.logging.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.garcia.models.Employee;
import dev.garcia.services.UserService;

public class EmployeeServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private UserService userService = new UserService();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		BufferedReader bw = request.getReader();
		String JSON = "";
		String line = bw.readLine();
		while(line != null) {
			JSON = JSON + line;
			line = bw.readLine();
		}
		
		ObjectMapper om = new ObjectMapper();
		Employee emp = om.readValue(JSON, Employee.class);
		String username = emp.getUsername();
		String password = emp.getPassword();
		int empID = userService.isEmployee(username, password);
		if(empID != -1) {	
			response.setStatus(200);
			response.addHeader("empID", String.valueOf(empID));
		}
		else 
			response.setStatus(400);
	}

	

}
