package dev.garcia.servlets;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.garcia.models.Manager;
import dev.garcia.services.UserService;

public class ManagerServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	private UserService userService = new UserService();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		BufferedReader bw = request.getReader();
		String JSON = "";
		String line = bw.readLine();
		while(line != null) {
			JSON = JSON + line;
			line = bw.readLine();
		}
		
		ObjectMapper om = new ObjectMapper();
		Manager m = om.readValue(JSON, Manager.class);
		String username = m.getUsername();
		String password = m.getPassword();
		boolean checker = userService.isManager(username, password);
		if(checker == true)
			response.setStatus(200);		
		else 
			response.setStatus(400);
	}

}
