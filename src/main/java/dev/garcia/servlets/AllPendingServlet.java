package dev.garcia.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.garcia.models.Request;
import dev.garcia.models.detailedRequest;
import dev.garcia.services.ManagerService;

public class AllPendingServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private ManagerService managerService = new ManagerService();
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	
		ArrayList<detailedRequest> allPendingList = managerService.allPendingView();
		ObjectMapper om = new ObjectMapper();
		String requestsJson = om.writeValueAsString(allPendingList);
		PrintWriter pw = response.getWriter();
		pw.write(requestsJson);
		pw.close();

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		BufferedReader bw = request.getReader();
		String JSON = "";
		String line = bw.readLine();
		while(line != null) {
			JSON = JSON + line;
			line = bw.readLine();
		}
		
		ObjectMapper om = new ObjectMapper();
		Request r = om.readValue(JSON, Request.class);
		int empID = r.getId();
		String status = r.getRequestStatus();
		if(status.equals("A"))
			managerService.updateStatusResolved(empID);
		else
			managerService.updateStatusDenied(empID);
		response.setStatus(200);
	}
}
