package dev.garcia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dev.garcia.models.Employee;
import dev.garcia.models.Request;
import dev.garcia.util.ConnectionUtil;

public class EmployeeDaoImpl implements EmployeeDao{

	@Override
	public void insertRequest(int requestID, double amount) {
		String insertString = "INSERT INTO REQUESTS_ERS (REQUEST_ID, AMOUNT, REQUEST_STATUS) VALUES (?, ?, 'PENDING')";
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement insertStatement = connection.prepareStatement(insertString)){
			
			insertStatement.setInt(1, requestID);
			insertStatement.setDouble(2, amount);
			insertStatement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public ArrayList<Request> pendingView(int requestID){
		String pendingViewString = "SELECT ID, AMOUNT FROM REQUESTS_ERS WHERE REQUEST_ID = ? AND REQUEST_STATUS = 'PENDING'";
		ArrayList<Request> requests = new ArrayList<Request>();
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pendingViewStatement = connection.prepareStatement(pendingViewString)){
			
			pendingViewStatement.setInt(1, requestID);
			ResultSet rs = pendingViewStatement.executeQuery();
			while(rs.next()){				
				int id = rs.getInt("ID");
				double amount = rs.getDouble("AMOUNT");
				String status = "PENDING";
				Request r = new Request(id, amount, status);
				requests.add(r);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return requests;
		
	}
	public ArrayList<Request> resolvedView(int requestID){
		String resolvedViewString = "SELECT ID, AMOUNT, REQUEST_STATUS FROM REQUESTS_ERS WHERE REQUEST_ID = ? AND REQUEST_STATUS != 'PENDING'";
		ArrayList<Request> requests = new ArrayList<Request>();
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement resolvedViewStatement = connection.prepareStatement(resolvedViewString)){
			
			resolvedViewStatement.setInt(1, requestID);
			ResultSet rs = resolvedViewStatement.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("ID");
				double amount = rs.getDouble("AMOUNT");
				String status = rs.getString("REQUEST_STATUS");
				Request r = new Request(id, amount, status);
				requests.add(r);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return requests;
	}
	
	public Employee employeeProfileView(String username){
		String profileViewString = "SELECT EMPLOYEE_ID, USERNAME, PASSWORD, FIRST_NAME, LAST_NAME FROM EMPLOYEE_ERS WHERE USERNAME = ?";
		Employee emp = new Employee();
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement profileViewStatement = connection.prepareStatement(profileViewString)){
			
			profileViewStatement.setString(1, username);
			ResultSet rs = profileViewStatement.executeQuery();
			rs.next();
			emp.setEmpID(rs.getInt("EMPLOYEE_ID"));
			emp.setUsername(rs.getString("USERNAME"));
			emp.setPassword(rs.getString("PASSWORD"));
			emp.setFname(rs.getString("FIRST_NAME"));
			emp.setLname(rs.getString("LAST_NAME"));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;

	}
	/*public void updateProfile(String username, String password, String fname, String lname) {
		String updateProfileString = "UPDATE EMPLOYEE_ERS SET USERNAME = ?, PASSWORD = ?, FIRST_NAME = ?, LAST_NAME = ?";
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement updateProfileStatement = connection.prepareStatement(updateProfileString)){
			
			updateProfileStatement.setString(1, username);
			updateProfileStatement.setString(2, password);
			updateProfileStatement.setString(3, fname);
			updateProfileStatement.setString(4, lname);
			updateProfileStatement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	*/
}
