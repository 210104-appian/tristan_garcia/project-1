package dev.garcia.daos;

public interface UserDao {
	public int isEmployee(String username, String password);
	public boolean isManager(String username, String password);

}
