package dev.garcia.daos;

import java.util.ArrayList;

import dev.garcia.models.Employee;
import dev.garcia.models.Request;

public interface EmployeeDao {
	public void insertRequest(int requestID, double amount);
	public ArrayList<Request> pendingView(int requestID);
	public ArrayList<Request> resolvedView(int requestID);
	public Employee employeeProfileView(String username);
	//public void updateProfile(String username, String password, String fname, String lname);
}
