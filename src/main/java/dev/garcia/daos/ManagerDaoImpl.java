package dev.garcia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dev.garcia.models.Employee;
import dev.garcia.models.detailedRequest;
import dev.garcia.util.ConnectionUtil;

public class ManagerDaoImpl implements ManagerDao{
	public ArrayList<detailedRequest> allPendingView(){
		
		String allPendingViewString = "SELECT USERNAME, FIRST_NAME, LAST_NAME, ID, AMOUNT, REQUEST_STATUS FROM my_view_ERS WHERE REQUEST_STATUS = 'PENDING'";
		ArrayList<detailedRequest> requests = new ArrayList<detailedRequest>();
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement allPendingViewStatement = connection.prepareStatement(allPendingViewString)){
			
			ResultSet rs = allPendingViewStatement.executeQuery();
			while(rs.next()) {
				detailedRequest dr = new detailedRequest();
				dr.setRequestID(rs.getInt("ID"));
				dr.setAmount(rs.getDouble("AMOUNT"));
				dr.setRequestStatus("PENDING");
				dr.setUsername(rs.getString("USERNAME"));
				dr.setFname(rs.getString("FIRST_NAME"));
				dr.setLname(rs.getString("LAST_NAME"));

				requests.add(dr);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return requests;
	}
	public void updateStatusResolved(int requestID) {
		String updateStatusResolvedString = "UPDATE REQUESTS_ERS SET REQUESTS_ERS.REQUEST_STATUS = 'RESOLVED' WHERE REQUESTS_ERS.ID = ?";
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement updateStatusResolvedStatement = connection.prepareStatement(updateStatusResolvedString)){
			
			updateStatusResolvedStatement.setInt(1,requestID);
			updateStatusResolvedStatement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void updateStatusDenied(int requestID) {
		String updateStatusDeniedString = "UPDATE REQUESTS_ERS SET REQUESTS_ERS.REQUEST_STATUS = 'DENIED' WHERE REQUESTS_ERS.ID = ?";
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement updateStatusDeniedStatement = connection.prepareStatement(updateStatusDeniedString)){
			
			updateStatusDeniedStatement.setInt(1,requestID);
			updateStatusDeniedStatement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public ArrayList<detailedRequest> allResolvedView(){
		String allResolvedViewString = "SELECT USERNAME, FIRST_NAME, LAST_NAME, ID, AMOUNT, REQUEST_STATUS FROM MY_VIEW_ERS WHERE REQUEST_STATUS = 'RESOLVED'";
		ArrayList<detailedRequest> requests = new ArrayList<detailedRequest>();
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement allResolvedViewStatement = connection.prepareStatement(allResolvedViewString)){
			
			ResultSet rs = allResolvedViewStatement.executeQuery();
			while(rs.next()) {
				detailedRequest dr = new detailedRequest();
				dr.setRequestID(rs.getInt("ID"));
				dr.setAmount(rs.getDouble("AMOUNT"));
				dr.setRequestStatus("RESOLVED");
				dr.setUsername("USERNAME");
				dr.setFname("FIRST_NAME");
				dr.setLname("LAST_NAME");
				
				requests.add(dr);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return requests;
		
	}
	/*public ArrayList<Employee> employeeView(){
		String allEmployeeString = "SELECT EMPLOYEE_ID, USERNAME, FIRST_NAME, LAST_NAME FROM EMPLOYEE_ERS ee";
		ArrayList<Employee> employees = new ArrayList<Employee>();
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement allEmployeeStatement = connection.prepareStatement(allEmployeeString)){
			
			ResultSet rs = allEmployeeStatement.executeQuery();
			while(rs.next()) {
				Employee e = new Employee();
				e.setEmpID(rs.getInt("EMPLOYEE_ID"));
				e.setUsername(rs.getString("USERNAME"));
				e.setPassword(rs.getString("PASSWORD"));
				e.setFname(rs.getString("FIRST_NAME"));
				e.setLname(rs.getString("LAST_NAME"));
				employees.add(e);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employees;
	}
	*/
}
