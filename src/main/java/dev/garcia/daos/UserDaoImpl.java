package dev.garcia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.juli.logging.Log;

import dev.garcia.util.ConnectionUtil;

public class UserDaoImpl implements UserDao{
	
	public int isEmployee(String username, String password) {
		String getEmployeeNum = "SELECT COUNT (*) FROM EMPLOYEE_ERS e WHERE e.USERNAME = ? AND e.PASSWORD = ?";
		String getEmployeeID = "SELECT EMPLOYEE_ID FROM EMPLOYEE_ERS e WHERE e.USERNAME = ? AND e.PASSWORD = ?";
		int employeeNum = 0;
		int empID = 0;
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement employeeNumStatement = connection.prepareStatement(getEmployeeNum);
				PreparedStatement employeeIDStatement = connection.prepareStatement(getEmployeeID)){
			employeeNumStatement.setString(1, username);
			employeeNumStatement.setString(2, password);
			ResultSet rs = employeeNumStatement.executeQuery();
			rs.next();
			employeeNum = rs.getInt("COUNT(*)");
			
			employeeIDStatement.setString(1, username);
			employeeIDStatement.setString(2, password);
			ResultSet rs2 = employeeIDStatement.executeQuery();
			rs2.next();
			empID = rs2.getInt("EMPLOYEE_ID");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(employeeNum == 1) 
			return empID;
		else
			return -1;
		
	}
	public boolean isManager(String username, String password) {
		String getManagerNum = "SELECT COUNT (*) FROM MANAGER_ERS e WHERE e.USERNAME = ? AND e.PASSWORD = ?";
		int managerNum = 0;
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement managerNumStatement = connection.prepareStatement(getManagerNum)){
			managerNumStatement.setString(1, username);
			managerNumStatement.setString(2, password);
			ResultSet rs = managerNumStatement.executeQuery();
			rs.next();
			managerNum = rs.getInt("COUNT(*)");
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(managerNum == 1)
			return true;
		else
			return false;
		
	}
}
