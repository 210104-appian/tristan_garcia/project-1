const baseUrl = "http://localhost:8080/java-ERS-system";
let currID = sessionStorage.getItem("empID");
const resolvedUrl = baseUrl + "/resolved?empID=" + currID;
let resolvedRequests;
var new_tbody = document.createElement('tbody');
var old_tbody = document.getElementById('myTBody2');

window.onload = function(){
    var xhr1 = new XMLHttpRequest();
    xhr1.open("GET", resolvedUrl);
    xhr1.onreadystatechange = function(){
        if(xhr1.readyState==4){
			resolvedRequests = JSON.parse(xhr1.response);
			for (let r of resolvedRequests) {
			let newRow = document.createElement("tr");
			let IDColumn = document.createElement("td");
  			IDColumn.innerText = r.id;
  			let requestAmount = document.createElement("td");
  			requestAmount.innerText = r.amount.toFixed(2);
			var statusColumn = document.createElement("td");
			statusColumn.innerText = r.requestStatus;
			newRow.append(IDColumn,requestAmount, statusColumn);
			new_tbody.appendChild(newRow);	
			}
		old_tbody.parentNode.replaceChild(new_tbody, old_tbody);
		}
    }
	xhr1.send();
}

signOutBtn.addEventListener("click", (e) => {
	sessionStorage.setItem("empID", "");
})
