const baseUrl = "http://localhost:8080/java-ERS-system";
const employeeUrl = baseUrl + "/employee";
const managerUrl = baseUrl + "/manager";


const loginForm = document.getElementById("login-form");
const loginButton = document.getElementById("login-form-submit");
const loginErrorMsg = document.getElementById("login-error-msg");

loginButton.addEventListener("click", (e) => {
    e.preventDefault();
    let currUsername = loginForm.username.value;
    let currPassword = loginForm.password.value;
	var userObj = { username: currUsername, password: currPassword};
    var myJSON = JSON.stringify(userObj);
    var xhr1 = new XMLHttpRequest();
    xhr1.open("POST", employeeUrl);
    xhr1.onreadystatechange = function(){
        if(xhr1.status == 200){
            let empID = xhr1.getResponseHeader("empID");
            sessionStorage.setItem("empID", empID);
 			window.location.replace("http://localhost:8080/java-ERS-system/static/employeeHomePage.html");
        }
        else{
            	var userObj = { username: currUsername, password: currPassword};
    			var myJSON = JSON.stringify(userObj);
    			var xhr2 = new XMLHttpRequest();
    			xhr2.open("POST", managerUrl);
    			xhr2.onreadystatechange = function(){
        		if(xhr2.status==200)
        			window.location.replace("http://localhost:8080/java-ERS-system/static/managerHomePage.html");
				else
					loginErrorMsg.style.opacity = 1;
    		}
    	xhr2.send(myJSON);
        }
    }
    xhr1.send(myJSON);
})

